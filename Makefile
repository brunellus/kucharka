all: kucharka.pdf

kucharka.pdf:
	mpost kunew.mp
	ps2pdf kunew.1
	ps2pdf kunew.2
	pdfcsplain -ini -enc plain-utf8-cs.tex
	pdfcsplain -fmt plain-utf8-cs kucharka.tex

clean:
	rm -f *.log kucharka.pdf plain-utf8-cs.fmt kunew.1 kunew.2 kunew.1.pdf kunew.2.pdf *.mpx
